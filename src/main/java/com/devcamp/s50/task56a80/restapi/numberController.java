package com.devcamp.s50.task56a80.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class numberController {
     @GetMapping("/checknumber")
     public String checkNumber(@RequestParam("number") int number) {
          if (number % 2 == 0) {
               return "Even";
          } else {
               return "Odd";
          }
     }
}
